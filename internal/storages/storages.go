package storages

import (
	"gitlab.com/lear4212225/go-rpc-auth/internal/db/adapter"
	"gitlab.com/lear4212225/go-rpc-auth/internal/infrastructure/cache"
	vstorage "gitlab.com/lear4212225/go-rpc-auth/internal/modules/auth/storage"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
