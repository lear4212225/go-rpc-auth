package grpc

import (
	"context"

	"gitlab.com/lear4212225/go-rpc-auth/internal/modules/auth/service"
)

type GRPCAuthService struct {
	authService service.Auther
}

func NewGRPCAuthService(authService service.Auther) *GRPCAuthService {
	return &GRPCAuthService{authService: authService}
}

func (g *GRPCAuthService) Register(ctx context.Context, in *RegisterIn) (*RegisterOut, error) {
	inService := service.RegisterIn{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	}

	outService := g.authService.Register(ctx, inService)

	out := RegisterOut{
		Status:    int32(outService.Status),
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCAuthService) AuthorizeEmail(ctx context.Context, in *AuthorizeEmailIn) (*AuthorizeOut, error) {
	inService := service.AuthorizeEmailIn{
		Email:          in.Email,
		Password:       in.Password,
		RetypePassword: in.RemessagePassword,
	}

	outService := g.authService.AuthorizeEmail(ctx, inService)
	out := AuthorizeOut{
		UserID:       int32(outService.UserID),
		AccessToken:  outService.AccessToken,
		RefreshToken: outService.RefreshToken,
		ErrorCode:    int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCAuthService) AuthorizeRefresh(ctx context.Context, in *AuthorizeRefreshIn) (*AuthorizeOut, error) {
	inService := service.AuthorizeRefreshIn{
		UserID: int(in.UserID),
	}

	outService := g.authService.AuthorizeRefresh(ctx, inService)
	out := AuthorizeOut{
		UserID:       int32(outService.UserID),
		AccessToken:  outService.AccessToken,
		RefreshToken: outService.RefreshToken,
		ErrorCode:    int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCAuthService) AuthorizePhone(ctx context.Context, in *AuthorizePhoneIn) (*AuthorizeOut, error) {
	inService := service.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int(in.Code),
	}

	outService := g.authService.AuthorizePhone(ctx, inService)
	out := AuthorizeOut{
		UserID:       int32(outService.UserID),
		AccessToken:  outService.AccessToken,
		RefreshToken: outService.RefreshToken,
		ErrorCode:    int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCAuthService) SendPhoneCode(ctx context.Context, in *SendPhoneCodeIn) (*SendPhoneCodeOut, error) {
	inService := service.SendPhoneCodeIn{
		Phone: in.Phone,
	}

	outService := g.authService.SendPhoneCode(ctx, inService)
	out := SendPhoneCodeOut{
		Phone: outService.Phone,
		Code:  int32(outService.Code),
	}
	return &out, nil
}

func (g *GRPCAuthService) VerifyEmail(ctx context.Context, in *VerifyEmailIn) (*VerifyEmailOut, error) {
	inService := service.VerifyEmailIn{
		Hash:  in.Hash,
		Email: in.Email,
	}

	outService := g.authService.VerifyEmail(ctx, inService)
	out := VerifyEmailOut{
		Success:   outService.Success,
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCAuthService) mustEmbedUnimplementedAuthServer() {
	//TODO implement me
	panic("implement me")
}
