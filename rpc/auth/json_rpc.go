package auth

import (
	"context"
	"log"

	"gitlab.com/lear4212225/go-rpc-auth/internal/modules/auth/service"
)

type AuthServiceJSONRPC struct {
	authService service.Auther
}

func NewAuthServiceJSONRPC(authService service.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{authService: authService}
}

func (t *AuthServiceJSONRPC) RegisterUser(in service.RegisterIn, out *service.RegisterOut) error {
	log.Printf("start auth register user rpc method")
	*out = t.authService.Register(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) AuthorizeEmail(in service.AuthorizeEmailIn, out *service.AuthorizeOut) error {
	*out = t.authService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) AuthorizeRefresh(in service.AuthorizeRefreshIn, out *service.AuthorizeOut) error {
	*out = t.authService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) AuthorizePhone(in service.AuthorizePhoneIn, out *service.AuthorizeOut) error {
	*out = t.authService.AuthorizePhone(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) SendPhoneCode(in service.SendPhoneCodeIn, out *service.SendPhoneCodeOut) error {
	*out = t.authService.SendPhoneCode(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) VerifyEmail(in service.VerifyEmailIn, out *service.VerifyEmailOut) error {
	*out = t.authService.VerifyEmail(context.Background(), in)
	return nil
}
